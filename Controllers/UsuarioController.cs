using System;
using System.Threading.Tasks;
using GerenciadorDeUsuarios.Models;
using GerenciadorDeUsuarios.Repositorios;
using Microsoft.AspNetCore.Mvc;

namespace GerenciadorDeUsuarios.Controllers
{
    public class UsuarioController : Controller
    {
        public IRepositorioBase<Usuario> Usuarios { get; }
        public UsuarioController(IRepositorioBase<Usuario> usuarios)
        {
            this.Usuarios = usuarios;
        }
        public async Task<IActionResult> Todos()
        {
            var usuarios = await Usuarios.GetALL();
            return View(usuarios);
        }

        public IActionResult Novo()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Novo([FromForm]Usuario usuario)
        {
            if(ModelState.IsValid)
            {
                await Usuarios.Save(usuario);
                return RedirectToAction("Todos");
            }
            else
            {
                return View();
            }                
            
        }

        public async Task<IActionResult> Editar(Guid id)
        {
            var usuario  = await Usuarios.GetById(id);
            return View(usuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Editar([FromForm]Usuario usuario, Guid id)
        {
            usuario.UsuarioId = id;
            await Usuarios.Update(usuario);
            return RedirectToAction("Todos");
        }

        public async Task<IActionResult> Deletar(Guid id)
        {
            var usuario  = await Usuarios.GetById(id);
            return View(usuario);
        }

        [HttpPost, ActionName("Deletar")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ConfirmacaoDeletar(Guid id)
        {
            await Usuarios.Delete(id);
            return RedirectToAction("Todos");
        }

    }   
}