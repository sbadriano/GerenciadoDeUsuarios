using System;

namespace GerenciadorDeUsuarios.Models
{
    public class PermissoesUsuario
    {
        public Guid UsuarioId { get; set; }
        public Usuario Usuario { get; set; }
        public Guid PermissaoId { get; set; }
        public Permissao Permissao { get; set; }
    }
}