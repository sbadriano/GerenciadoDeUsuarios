using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GerenciadorDeUsuarios.DbContexts;
using GerenciadorDeUsuarios.Models;
using Microsoft.EntityFrameworkCore;

namespace GerenciadorDeUsuarios.Repositorios
{
    public class Usuarios : IRepositorioBase<Usuario>
    {
        public async Task Delete(Guid id)
        {
            using(var db = new GerenciadorDeUsuariosContext())
            {
                var Usuario = await db.Usuarios.FindAsync(id);
                db.Usuarios.Remove(Usuario);
                await db.SaveChangesAsync();
            }
        }

        public async Task<IList<Usuario>> GetALL()
        {
            using(var db = new GerenciadorDeUsuariosContext())
            {
                return await db.Usuarios.ToListAsync();
            }   
        }

        public async Task<Usuario> GetById(Guid id)
        {
            using(var db = new GerenciadorDeUsuariosContext())
            {
                return await db.Usuarios.FindAsync(id);
            }
        }

        public async Task Save(Usuario obj)
        {
            using(var db = new GerenciadorDeUsuariosContext())
            {
                await db.AddAsync(obj);
                await db.SaveChangesAsync();
            }
        }

        public async Task Update(Usuario obj)
        {
            using(var db = new GerenciadorDeUsuariosContext())
            {
                db.Usuarios.Update(obj);
                await db.SaveChangesAsync();
            }
        }
    }
}